select
c.category_name
	sum(i.item_price)AS total_price
 from
 	item i
 inner join
 	item_category c
 on
 	i.category_id = c.category_id
 GROUP BY
	c.category_name
ORDER BY
   total_price;
